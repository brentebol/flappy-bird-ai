# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 16:18:44 2019

@author: brent
"""
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

global device
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class Brain(nn.Module):
    def __init__(self, num_input=6, num_output = 1):
        super(Brain, self).__init__()
        self.fc1 = nn.Linear(num_input, 4, bias=False)
        self.fc2 = nn.Linear(4,4, bias=False)
        self.fc3 = nn.Linear(4,3, bias=False)
        self.fc4 = nn.Linear(3, num_output, bias=False)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = F.sigmoid(self.fc4(x))
        return x

def Get_FCNet(number):
    lst = [0] * number
    for i in range(number):
        lst[i] = FCNet().__class__().cuda()
    return lst

def amount_param(lst):
    lst = [6] + lst + [1]
    amount = len(lst) - 1
    z = 0
    for i in range(amount):
        z += lst[i] * lst[i+1]
    return z

def get_best(birds):
    best_bird = birds[0]
    for bird in birds:
        if bird.score > best_bird.score:
            best_bird = bird
            
    return bird
    
def save_birds(population):
    for i, bird in enumerate(population):
        point = bird.pipes_passed
        torch.save(bird.nn.state_dict, "birds/bird%d(%d).pth" % (i,point))

















