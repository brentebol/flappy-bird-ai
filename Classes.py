# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 15:42:41 2019

@author: brent
"""
import numpy as np
import pygame as pg
import random as rnd
import torch
import matplotlib.pyplot as plt

from Neural_network_flappy import Brain


use_gpu = torch.cuda.is_available()
if use_gpu:
    device = torch.device('cuda:0' if use_gpu else 'cpu')


class Bird(): 
    def __init__(self, net = False):
        self.y = float(400)
        self.x = 100
        self.velocity = 0
        self.accel = 0.2
        self.is_alive = True
        
        self.pipes_passed = 0
        self.score = 0
        self.fit = 0
        if net:
            self.nn = net
        else:
            self.nn = Brain().cuda()
        
        #pygame variables
        self.color = (255,255,255)
        self.size = 10
    
    def jump(self):
        self.velocity = -6
        
    def update(self):
        self.velocity += self.accel
        self.y += self.velocity
        
        if self.y < 0:
            self.y = 0
            self.velocity = 0
        
    def check_collision(self, pipe, reso):
        if self.y >= reso[1]:
            return True
        if self.y >= pipe.h2 and (self.x <= pipe.x2 and self.x >= pipe.x1):
            return True
        elif self.y <= pipe.h1 and (self.x <= pipe.x2 and self.x >= pipe.x1):
            return True
    
    def Get_NN_Input(self, pipe, reso):
        inputs = [0]*6
        inputs[0] = (pipe.h1 - self.y) / reso[1] #vertical distance bird - top of gap
        inputs[1] = (pipe.h2 - self.y) / reso[1] #vertical dist bird - bottom of gap
        inputs[2] = (pipe.x1 - self.x) / reso[0] #horizontal dist bird - begin of pipe
        inputs[3] = (pipe.x2 - self.x) / reso[0] #horizontal dist bird - end of pipe
        inputs[4] = (reso[1] - self.y) / reso[1] #vertical dist bird - bottom of screen
        inputs[5] = self.velocity / 50  #arbitrary scaling factor
        return inputs
    
class Pipe():
    def __init__(self,reso):
        self.width = 15
        self.gap = 100
        self.speed = -1
        self.x1 = reso[0]
        self.x2 = self.x1 + self.width
        self.h1 = int(np.random.random() * (reso[1] - self.gap))
        self.h2 = self.h1 + self.gap
        
        self.new_possible = True
        
        #pygame variable
        self.color = (255,255,255)
        self.maxima = (0, reso[1])
        
    def move(self):
        self.x1 = self.x1 + self.speed
        self.x2 = self.x1 + self.width
        
    def remove(self):
        if self.x2 < 0:
            return True
        else:
            return False
        
    def make_new(self):
        if self.x2 < 125 and self.new_possible == True:
            self.new_possible = False
            return True
        else:
            return False
    
def Get_Amount(add_class ,number):
    lst = [0] * number
    for i in range(number):
        lst[i] = add_class.__class__()
    return lst
        
def Get_Next(pipes):
    if len(pipes) == 1:
        return pipes[0]
    elif pipes[0].x2 > 95:
        return pipes[0]
    else:
        return pipes[1]
    
def Pipes_passed(pipes):
    if pipes[0].x2 == 100:
        return True
    else:
        return False

def load_bird(path):
    bird = Bird()
    bird.nn.load_state_dict(torch.load(path))
    return bird

    
#%% BELOW here only metrics and plotting of results.
def Get_Jump_Map_velocity(bird, reso):
    scaling_factor = 1
    max_x = 300 + 1
    max_y = reso[1]
    V = [-6, -3, -1, 0, 1, 3, 6, 10]
    output = np.zeros((len(V), max_y, max_x))
    pipe = Pipe(reso)
    pipe.x2 = max_x
    pipe.x1 = pipe.x2 - pipe.width
    pipe.h1 = reso[1]/2 - pipe.gap / 2
    pipe.h2 = pipe.h1 + pipe.gap
    
    for x in range(0, max_x, scaling_factor):
        print( str(int(x/max_x * 100)) + "%" )
        for y in range(0, max_y, scaling_factor):
            if x > pipe.x1 and (y < pipe.h1 or y > pipe.h2):
                output[:, y, x] = 0.5
            else:
                for i in range(len(V)):
                    bird.x = x
                    bird.y = y
                    bird.velocity = V[i]
                    NN_input = bird.Get_NN_Input(pipe, reso)
                    NN_output = torch.Tensor(NN_input).view(-1, len(NN_input)).to(device)
                    output[i, y, x] = float(bird.nn(NN_output))
                
    normal = output.copy()
    normal[normal > 0.5] = 1
    normal[normal < 0.5] = 0   
    
    np.save("velocity_output", output)
    np.save("velocity_normal", normal)
    try:
        #plot_outputs(normal, V, "V")     #comments this if not doing the plotting stuff.
        pass
    except:
        print("Plotting failed")
    return output, normal
    
def Get_Jump_Map_pipes(bird, reso):
    gaps = [300, 75, 525, 150, 450]
    
    scaling_factor = 1
    max_x = reso[0] - 100 + 1 #resolution is inclusive [0,400] - Only check for bird(100px) to max_x(400px)
    max_y = reso[1] + 1 #resolution is inclusive [0,600]
    V = 0
    output = np.zeros((len(gaps), max_y, max_x))
    pipes = []
    for gap_loc in gaps:
        pipe = Pipe(reso)
        pipe.x2 = max_x
        pipe.x1 = pipe.x2 - pipe.width
        pipe.h1 = gap_loc - pipe.gap / 2
        pipe.h2 = gap_loc + pipe.gap / 2
        pipes.append(pipe)
        
    for x in range(0, max_x, scaling_factor):
        print( str(int(x/max_x * 100)) + "%" )
        for y in range(0, max_y, scaling_factor):
            for i, pipe in enumerate(pipes):
                if x > pipe.x1 and (y < pipe.h1 or y > pipe.h2):
                    output[i, y, x] = 0.5
                else:
                    bird.x = x
                    bird.y = y
                    bird.velocity = V
                    NN_input = bird.Get_NN_Input(pipe, reso)
                    NN_output = torch.Tensor(NN_input).view(-1, len(NN_input)).to(device)
                    output[i, y, x] = float(bird.nn(NN_output))
                
    normal = output.copy()
    normal[normal > 0.5] = 1
    normal[normal < 0.5] = 0       
    
    np.save("pipes_output", output)
    np.save("pipes_normal", normal)
    try:
        #plot_outputs(normal, gaps, "gaps")     #comments this if not doing the plotting stuff.
        pass
    except:
        print("Plotting failed")
    return output, normal
                

def plot_scores(ga_scores, ga_points,sorted_fit, idx, parent_fit, save = True):
    plt.clf() #clear the current plot
    plt.subplot(311)
    #plt.title("Maximum score & mean score")
    plt.xlabel("Generations [-]")
    plt.ylabel("Score [-]")
    
    plt.plot(ga_scores)
    plt.legend(["Maximum score", "Average score"])
    plt.draw()
    
    plt.subplot(312)
    #plt.title("Maximum score & mean score")
    plt.xlabel("Generations [-]")
    plt.ylabel("Points [-]")
    
    plt.plot(ga_points)
    plt.legend(["Maximum Point", "Average Points"])
    plt.draw()

    plt.subplot(313)
    plt.xlabel("Birds sorted by fit")
    plt.ylabel("Fitness [-]")
    
    plt.plot(sorted_fit, 'bo')
    plt.plot(idx, parent_fit, 'rs')
    plt.legend(["Population", "Parents"], loc= 'upper left')
    plt.draw()
    if save:
        plt.savefig("scores.eps", format="eps")
    

def plot_outputs(output, labels, name):
    #V = [-10, -6, -3, -1, 0, 1, 3, 6]
    #gaps = [300, 75, 525, 150, 450]
    #labels = V
    num = len(output[:,0,0])
    for i in range(num):
        plt.figure(i)
        plt.clf()
        plt.ylabel("pixels")
        plt.xlabel("pixels")
        plt.title("Speed: " + str(labels[i]))
        plt.imshow(output[i,:,:], cmap='bwr')
        plt.xticks([0,100,200,300])
        plt.show()
        #plt.savefig("Plaatjes/"+ name + str(labels[i]) + ".eps", format="eps")
        #plt.savefig("Plaatjes/"+ name + str(labels[i]) + ".png")
        
def plot_histograms(hist):
    num = len(hist[:,0])
    amount = len(hist[0,:])
    total = sum(hist[1,:])
    for i in range(num):
        plt.figure(i)
        plt.clf()
        plt.title("generation "+str(i))
        plt.ylabel("Amount of birds")
        plt.xlabel("Points")
        plt.ylim(0,total)
        plt.bar(range(amount), hist[i,:])
        plt.savefig("histogrammen/gen"+ str(i) + ".eps", format="eps")
        plt.savefig("histogrammen/gen"+ str(i) + ".png")
        
def plot_scores_2(ga_scores, ga_points,sorted_fit, idx, parent_fit, save = True):
    plt.clf() #clear the current plot
    plt.subplot(211)
    #plt.title("Maximum score & mean score")
    plt.xlabel("Generations [-]")
    plt.ylabel("Score [-]")
    
    plt.plot(ga_scores)
    plt.legend(["Maximum score", "Average score"])
    plt.draw()
    
    plt.subplot(212)
    #plt.title("Maximum score & mean score")
    plt.xlabel("Generations [-]")
    plt.ylabel("Points [-]")
    
    plt.plot(ga_points)
    plt.legend(["Maximum Point", "Average Points"])
    plt.draw()

    if save:
        plt.savefig("scores_only.eps", format="eps")       
